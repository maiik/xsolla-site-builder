/* global _*/
// 'use strict';



define([

], function () {

  function ColorPicker(parent, el, onChangeCallback) {
    this.onChangeCallback = onChangeCallback || null; //(this.pickerName, this.defColor)
    this._panel = parent;
    this._xsb = parent._xsb;
    this.el = el;
    // this.elContName = '.xsb_sett_colorpicker_parent';
    this.elContName = '.xsb_sett_colorpicker_parent';
    this.pickerName = this.el.dataset.xsbControlPanelGlobalColor;
    this.key = this.getKey();
    this.defColor = this.getDefColor();
    this.inpt = this.getInput();
    this.subm = this.getSubmit();
    this.picker = this.getPicker();
    this.setColor();
  }





  ColorPicker.prototype.getType = function () {
    if (this.el.xsbControlPanelGlobalRoundness) {
      return 'range'
    } else {
      return 'colorPicker'
    }
  }


  ColorPicker.prototype.getPicker = function () {

    jQuery.event.special.touchstart = {
      setup: function( _, ns, handle ){
        if ( ns.includes("noPreventDefault") ) {
          this.addEventListener("touchstart", handle, { passive: false });
        } else {
          this.addEventListener("touchstart", handle, { passive: true });
        }
      }
    };


    var picker = $(this.el).find('[data-xsb-settings-panel-global-color=\'picker\']')[0];
    var thiss = this;
    $(picker).spectrum({
      // flat: true,
      containerClassName: 'xsb_color_picker',
      // appendTo: thiss.el,
      appendTo: '.xsb_sett_colorpicker_parent',
      showAlpha: true,
      allowAlpha: true,
      color: this.defColor,
      clickoutFiresChange: true,
      // allowEmpty:true,
      // showButtons: false,
      // showInitial: true,
      showInput: true,
      preferredFormat: 'rgba',
      // showPalette: true,
      // palette: [
      //     ['black', 'white', 'blanchedalmond'],
      //     ['rgb(255, 128, 0);', 'hsv 100 70 50', 'lightyellow']
      // ],
      change: function(color) {
        // $("#basic-log").text(color.toHexString());
        // thiss.defColor = color.toHexString();
        thiss.defColor = color.toRgbString();
        thiss.setColor();
      }
    });
    $(picker).on('move.spectrum', function(e, color) {
      thiss.defColor = color.toRgbString();
      thiss.setColor();
    });
    $(picker).on('beforeShow.spectrum', function(e, tinycolor) { thiss.zShowHide('shown') });
    $(picker).on('hide.spectrum', function(e, tinycolor) { thiss.zShowHide('hidden')});

    return picker;
  }


  ColorPicker.prototype.zShowHide = function (doo) {
    var $c = $('.xsb_sett_colorpicker_parent');
    var $z = $('.xsb_sett_colorpicker_z');
    switch (doo) {
      case 'shown':
        $z.addClass('shown');
        $c.addClass('shown');
        $('.xsb_color_picker').removeClass('shown');
        // $('.xsb_color_picker').removeClass('shown');
        $(this.elContName).addClass('shown');
        break;
      case 'hidden':
        $z.removeClass('shown');
        $c.removeClass('shown');
        // $(this.elContName).removeClass('shown');
        $('.xsb_color_picker').removeClass('shown');
        break;
    }
  }


  ColorPicker.prototype.getDefColor = function () {
    if (!this._xsb.dataX.global.userTheme[this.pickerName]) {
      this._xsb.dataX.global.userTheme[this.pickerName] = '#FF0000';
    };
    return this._xsb.dataX.global.userTheme[this.pickerName] || '#FF0000';
  }


  ColorPicker.prototype.getKey = function () {
    if (this.pickerName === 'textcolor') {
      return 'color';
    }

    if (this.pickerName === 'accent') {
      return 'background-color';
    }

    if (this.pickerName === 'accent_text') {
      return 'color';
    }

    if (this.pickerName === 'back') {
      return 'background-color';
    }
  }

  ColorPicker.prototype.getInput = function () {
    return $(this.el).find('[data-xsb-settings-panel-global-color="input"]')[0];
  }

  ColorPicker.prototype.getSubmit = function () {
    return $(this.el).find('[data-xsb-settings-panel-global-color="submit"]')[0];
  }


  ColorPicker.prototype.setColor = function () {
    // this._xsb.dataX.global.userTheme[this.pickerName][this.getKey()] = this.defColor;
    // this._xsb.setData();
    if (this.onChangeCallback) this.onChangeCallback(this.pickerName, this.defColor);
    if (this.inpt && this.defColor) {
      // this.inpt.innerHTML = m__rgbToHexAlpha(this.defColor)[0];
      this.inpt.textContent = m__rgbToHexAlpha(this.defColor)[0];
    }

    // this._xsb.setCustomTheme(this.pickerName, this.defColor);

  }





  return ColorPicker;
})