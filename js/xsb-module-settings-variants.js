/* global _*/
// 'use strict';



define([
], function () {



  /********************* MODULE VARIANTS *********************/
  /********************* MODULE VARIANTS *********************/
  /********************* MODULE VARIANTS *********************/
  /********************* MODULE VARIANTS *********************/



  function ModuleVariantController(parent, moduleName, el, callback, callbackScope) {
    this._parent = parent;
    this.callback = callback;
    this.callbackScope = callbackScope;
    this._xsb = this._parent._xsb;
    this.moduleName = moduleName;
    this.dataV = this._xsb.dataX['modules'][this.moduleName];
    this.el = el;
    this.elVars = this.getElVars(el);
    // this.template = this.getTemplate();
    this.thiss = null;
    this.addListeners();
  }


  ModuleVariantController.prototype.getElVars = function (_parent) {
    var elVars = {}
    var thiss = this;
    $(this.el).find(':attr(\'^data-xsb-control-panel-module-variant\')').each(function (i, oneVar) {
      var varName = oneVar.dataset.xsbControlPanelModuleVariant;
      var act = (thiss.dataV['variant'] === varName) ? true : false;
      elVars[varName] = {
        // 'active': act,
        'varName': varName,
        'el': oneVar,
      }
      if (act) { $(oneVar).find('input:radio:first').prop('checked', true) };
    })
    return elVars;
  }

  // ModuleVariantController.prototype.getTemplate = function () {
  //   return this._xsb.templates[this.moduleName];
  // }





  ModuleVariantController.prototype.addListeners = function () {
    var thiss = this;
    for (var one in this.elVars) {
      var oneVar = this.elVars[one];
      $(oneVar['el']).on({
        click: function (evt) {
          // evt.preventDefault();
          // evt.stopPropagation();
          thisVar = thiss.elVars[this.dataset.xsbControlPanelModuleVariant];
          $(thisVar['el']).find('input:radio:first').prop('checked', true);
          // thisVar['active'] = true;
          // var varName = this.dataset.xsbControlPanelModuleVariant;
          thiss.setVariant(thisVar['varName']);
        }
      })
    }
  }





  ModuleVariantController.prototype.setVariant = function (newVar) {
    console.log('this = ', this.moduleName);
    this.dataV['variant'] = newVar;
    this._xsb.setData();
    // this.template.setActiveVar(newVar);
    if (this.callback) this.callback(newVar, this.callbackScope);
  }

  return ModuleVariantController;
})