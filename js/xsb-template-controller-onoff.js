/* global _*/
// 'use strict';



define([

], function () {



  /********************* TemplateControllerOnOff *********************/
  /********************* TemplateControllerOnOff *********************/
  /********************* TemplateControllerOnOff *********************/
  /********************* TemplateControllerOnOff *********************/



  function TemplateControllerOnOff(_parent, contName) {
    this._template = _parent;
    this._xsb = this._template._xsb;
    this.contName = contName;
    this.$el = this.getEl();
    this.$trigger = this.getTrigger();
    this.$triggerActiveEl = this.triggerActiveEl();
    this.dataT = this.getData();
    // this.active = this.getActive();
    this.radio = this.getRadio();
    this.setActive(this.getActive());
    this.addListener();
  }


  TemplateControllerOnOff.prototype.triggerActiveEl = function () {
    var $triggerActiveEl = $('[data-xsb-settings-module-onoff-activetarget=\'' + this.contName + '\']') || null;
    return $triggerActiveEl;
  }


  TemplateControllerOnOff.prototype.getRadio = function () {
    if (this.radio) return this.radio;
    var $radio = this.$trigger.find('input[type=radio]');
    if (!$radio.length) $radio = this.$trigger.find('input[type=checkbox]');
    if (!$radio.length) return;
    var thiss = this;
    var groupName = $radio[0].name;

    $radio.change(function(){
      var radioValue = $radio.filter(':checked').val();
      thiss._template.onOffSwitch(thiss.getRadioGroupName(), radioValue);
      thiss._xsb.setData();
    });
    return $radio;
  }


  TemplateControllerOnOff.prototype.getRadioGroupName = function () {
    var radioGroupName = this.$trigger.find('input[type=radio]').attr('name') || this.$trigger.find(':checkbox').attr('name') || null;
    return radioGroupName;
  }



  TemplateControllerOnOff.prototype.getData = function () {
    if (!this._template.dataT['config']) this._template.dataT['config'] = {};
    if (!this._template.dataT['config'][this.contName]) this._template.dataT['config'][this.contName] = {};
    return this._template.dataT['config'][this.contName];
  }


  TemplateControllerOnOff.prototype.getEl = function () {
    // return new Template(this, this.contName);
    // return $(this._template.el).find('[data-xsb-template-onoff=\'' + this.contName + '\']');
    return $('[data-xsb-template-onoff=\'' + this.contName + '\']'); //TODO: прочекать, выключение
    // return $(':attr(\'^data-attr="' + this.contName + '"\')');
  }

  TemplateControllerOnOff.prototype.getTrigger = function () {
    // return new Template(this, this.contName);
    return $('[data-xsb-settings-module-onoff=\'' + this.contName + '\']');
  }


  TemplateControllerOnOff.prototype.getActive = function (state) {
    var state;
    if (typeof this.dataT['shown'] === 'undefined') {
      if (this.$el.data('xsbTemplateOnoffInitial')) {
        state = true;
      } else {
        state = false;
      }
    } else {
      state = this.dataT['shown'];
    }
    return state;
  }



  TemplateControllerOnOff.prototype.setActive = function (state) {
    state = (typeof state !== 'undefined') ? state : this.getActive();
    this.dataT['shown'] = state;
    this.updateState();
  }


  TemplateControllerOnOff.prototype.setChecked = function (state) {
    this.$trigger.find('input:radio:first').prop('checked', state);
    this.$trigger.find('input:checkbox').prop('checked', state);
  }



  TemplateControllerOnOff.prototype.updateState = function () {
    if (this.getActive()) {
      this.$el.removeClass('hidden');
      this.$trigger.addClass('active');
      this.$triggerActiveEl.addClass('active');
      // if (!this.$trigger.find('input:radio:first').prop('checked')) {
        // this.$trigger.find('input:radio:first').prop('checked', true);
      // }
    } else {
      this.$el.addClass('hidden');
      this.$trigger.removeClass('active');
      this.$triggerActiveEl.removeClass('active');
      // if (!this.$trigger.find('input:radio:first').prop('checked')) {
        // this.$trigger.find('input:radio:first').prop('checked', false);
      // }
    }
    this.setChecked(this.getActive());
    // this.$trigger.find('input:radio:first').prop('checked', this.getActive());
    // this.$trigger.find('input:checkbox:first').prop('checked', this.getActive());
  }




  TemplateControllerOnOff.prototype.addListener = function () {

    // if (this.radio) return;

    var thiss = this;
    this.$trigger.on({
      // change: function (evt) {
      click: function (evt) {
        console.log('this = ', this);
        var inp = thiss.$trigger.find('input');
        var newState = false;
        if ($(this).find('input:checked').length || $(this).parent().find('input:checked').length) newState = true;
        // if (!thiss.dataT['shown']) newState = true;
        thiss.setActive(newState);
        thiss._xsb.setData();
      }
    });
  }






  TemplateControllerOnOff.prototype.showHideAdditionalSettings = function () {

    //Show add settings
    $('.xsb_sett').addClass('shown');
    setTimeout(function () {
      $('.xsb_sett_b, .xsb_z').addClass('shown');
    }, 1);

    //Show add settings
    $('[data-xsb-settings-panel=\'modules\']').find('.xsb_settings_inner').addClass('moved');
    $(':attr(\'^data-xsb-control-module\')').addClass('hidden'); //TODO: getacрятать по-умному
    this.addSettings.showHide('show');
  }



  return TemplateControllerOnOff;
})