/* global _*/
// 'use strict';



define([

], function () {



  function ColorPickerMedia(_parent, el) {
    this._parent = _parent;
    this.onChangeCallback = _parent.onTintChangeCallback || null; //(this.pickerName, this.defColor)
    this._xsb = _parent._xsb;
    this.el = el;
    // this.key = this.getKey();
    // this.pickerName = this.el.dataset.xsbControlPanelGlobalColor;
    // this.defColor = this.getDefColor();
    // this.inpt = this.getInput();
    // this.subm = this.getSubmit();
    this.picker = this.getPicker();
    // this.setColor();
  }





  ColorPickerMedia.prototype.getPicker = function () {
    var thiss = this;
    var picker;
    if (this.el.dataset.xsbSettingsMediaTint === 'picker') {
      picker = $(this.el).find('[data-xsb-settings-media-tint-color=\'picker\']');
      // picker = this.el;
    }
    if (!picker) {
      // picker = $(this.el).find(':attr(\'^data-xsb-settings-media-tint\')')[0];
      picker = thiss.el;
      var pickerColor = picker.dataset.xsbSettingsMediaTint;
      $(picker).css({ 'background-color': pickerColor });
      $(picker).on({
        click: function (evt) {
          thiss.setColor(pickerColor);
          thiss._parent.colorPicker['picker'].setPickerColor(pickerColor)
        },
        // mouseover: function (evt) {
        //   thiss.setImage(oneMediaId);
        // },
        // mouseout: function (evt) {
        //   thiss.setImage();
        // },
      });
      return picker;
    }


    $(picker).spectrum({
      // flat: true,
      containerClassName: 'xsb_color_picker',
      // appendTo: thiss.el,
      appendTo: thiss.el,
      showAlpha: true,
      allowAlpha: true,
      color: (thiss._parent.targetMedia) ? thiss._parent.targetMedia.getTint() : 'rgba(0, 0, 0, 0.5)',
      clickoutFiresChange: true,
      // allowEmpty:true,
      // showButtons: false,
      // showInitial: true,
      showInput: true,
      preferredFormat: 'rgba',
      // showPalette: true,
      // palette: [
      //     ['black', 'white', 'blanchedalmond'],
      //     ['rgb(255, 128, 0);', 'hsv 100 70 50', 'lightyellow']
      // ],
      change: function(color) {
        // $("#basic-log").text(color.toHexString());
        // thiss.defColor = color.toHexString();
        // thiss.defColor = color.toRgbString();
        thiss.setColor(color.toRgbString());
      }
    });
    $(picker).on({
      'move.spectrum': function(e, color) {
        // thiss.defColor = color.toRgbString();
        // thiss.setColor();
        thiss.setColor(color.toRgbString());
      },
    });
    return picker;
  }


  ColorPickerMedia.prototype.setPickerColor = function (newColor) {
    newColor = newColor || (this.targetMedia) ? this.targetMedia.getTint() : null;
    $(this._parent.colorPicker['picker'].picker).spectrum('set', newColor);
    this.hidePicker();
  }

  ColorPickerMedia.prototype.hidePicker = function () {
    $(this._parent.colorPicker['picker'].picker).spectrum('hide');
  }



  ColorPickerMedia.prototype.getKey = function () {
    if (this.pickerName === 'textcolor') {
      return 'color';
    }

    if (this.pickerName === 'accent') {
      return 'background-color';
    }

    if (this.pickerName === 'accent_text') {
      return 'color';
    }

    if (this.pickerName === 'back') {
      return 'background-color';
    }
  }

  ColorPickerMedia.prototype.getInput = function () {
    return $(this.el).find('[data-xsb-settings-panel-global-color="input"]')[0];
  }

  ColorPickerMedia.prototype.getSubmit = function () {
    return $(this.el).find('[data-xsb-settings-panel-global-color="submit"]')[0];
  }


  ColorPickerMedia.prototype.setColor = function (newColor) {
    var thiss = this;
    if (this._parent.onChangeCallback) thiss._parent.onChangeCallback(newColor);
  }





  return ColorPickerMedia;
})