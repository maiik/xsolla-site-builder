/* global _*/
// 'use strict';



define([
    // 'js/xsb-global_background',
], function (XsollaSiteBuilder) {




  /********************* BACKGROUND *********************/
  /********************* BACKGROUND *********************/
  /********************* BACKGROUND *********************/
  /********************* BACKGROUND *********************/




  function Background(parent, el) {
    this._xsb = parent._xsb;

    this.dataG = this._xsb.dataX['global'];

    this.$el = $('[data-xsb=\'bg\']');
    this.$elVideo = $('[data-xsb-bg=\'youtube\']');
    this.$elPic = $('[data-xsb-bg=\'pic\']');
    this.$elDark = $('[data-xsb-bg=\'tint\']');
    this.$elCtrl = $('[data-xsb-settings-global=\'bg\']');
    this.$elCtrlInput = this.$elCtrl.find('[data-xsb-control-panel-global-bg=\'url\']');
    this.$elCtrlSubmit = this.$elCtrl.find('[data-xsb-control-panel-global-bg=\'submit_url\']');
    this.$elCtrlChecks = this.$elCtrl.find(':attr(\'^data-xsb-control-panel-global-bg-check\')');

    this.setController()
    this.onYouTubeIframeAPIReady();
  }

  Background.prototype.setController = function () {
    //Set Input Value From Data
    var $url = this.$elCtrlInput;
    $url.val(this.dataG['video']['id']);
    var thiss = this;

    this.$elCtrl.find('input:checkbox').each(function (i, oneCheck) {
      var checkName = $(oneCheck).closest(':attr(\'^data-xsb-control-panel-global-bg-check\')')[0].dataset.xsbControlPanelGlobalBgCheck;
      $(oneCheck).prop('checked', thiss.dataG[checkName]['shown']);
    })


    this.$elCtrlSubmit.on({
        click: function (evt) {
          //Set media from Url
          evt.preventDefault();
          if (evt.target.dataset.xsbControlPanelGlobalBg === 'submit_url' ||
          evt.target.parentElement.dataset.xsbControlPanelGlobalBg === 'submit_url') {
            var url = $('[data-xsb-control-panel-global-bg=\'url\']')[0].value;
            if (!url) return;
            thiss.setBg(url);
          };
        }
    });

    this.$elCtrlChecks.on({
      click: function (evt) {
        //turn on video

        thiss.$elCtrlChecks.each(function (i, item) {
          var whatIsControlled = item.dataset.xsbControlPanelGlobalBgCheck;
          var val = $(item).find('input:checkbox:first').attr('checked', 'checked')[0].checked;
          thiss.dataG[whatIsControlled]['shown'] = val;
        })
        thiss.setBg();
        // }
      }
    });
  }



  Background.prototype.setBg = function (picVideoUrl) {

    var newPicUrl = null;
    var newVideoId = null;




    if (this.dataG['video']) {
      newVideoId = this.youtubeGetIdFromURL(this.dataG['video']['id']);
      if (this.dataG['video']['shown']) {
        this.$elVideo.removeClass('hidden');
      } else {
        this.$elVideo.addClass('hidden');
      }
    }


    // if (!picVideoUrl) {
    //   newPicUrl = this.dataG['pic']['url'];
    //   newVideoId = this.dataG['video']['id'];
    // } else {
    //   if (this.checkPicUrl(picVideoUrl)) {
    //     // newPicUrl = picVideoUrl;
    //     // this.dataG['pic']['url'] = newPicUrl;
    //   } else {
    //     newVideoId = this.youtubeGetIdFromURL(picVideoUrl);
    //     this.dataG['video']['id'] = newVideoId;
    //   }
    // }

    newVideoId = (this.dataG['video']) ? this.youtubeGetIdFromURL(this.dataG['video']['id']) : null;
    newPicUrl = (this.dataG['media']['bg']) ? this.dataG['media']['bg']['url'] : null;
    this._xsb.setData('global');



    if (newPicUrl) { //pic
      this.$elPic.css('background-image', 'url(\'' + newPicUrl + '\')');
    }

    if (newVideoId){ //video
      //Видео

      // $('.xsb_youtubebg').removeClass('hidden');

      //Set video bg
      var prnt = $('#muteYouTubeVideoPlayer')[0].parentElement;
      // if (!picVideoUrl) {
      //   prnt.classList.add('hidden');
      //   return;
      // } else {
      //   prnt.classList.remove('hidden');
      // }
      if ($('#muteYouTubeVideoPlayer').length) {
        prnt.removeChild($('#muteYouTubeVideoPlayer')[0])
      }
      var newDiv = document.createElement("div");
      newDiv.id = 'muteYouTubeVideoPlayer';
      prnt.appendChild(newDiv);
      // xsb_bg_player = '';

      if (!debug) this.xsb_bg_player = new YT.Player('muteYouTubeVideoPlayer', {
        //videoId: 'W0LHTWG-UmQ', // YouTube Video ID
        videoId: newVideoId, // YouTube Video ID
        width: 560,               // Player width (in px)
        height: 316,              // Player height (in px)
        playerVars: {
          playlist: newVideoId,
          autoplay: 1,        // Auto-play the video on load
          controls: 1,        // Show pause/play buttons in player
          showinfo: 0,        // Hide the video title
          modestbranding: 1,  // Hide the Youtube Logo
          loop: 1,            // Run the video in a loop
          fs: 0,              // Hide the full screen button
          cc_load_policy: 0, // Hide closed captions
          iv_load_policy: 3,  // Hide the Video Annotations
          autohide: 0         // Hide video controls when playing
        },
        events: {
          onReady: function(e) {
            e.target.mute();
          }
        }
      });
    }
  }


  Background.prototype.checkPicUrl = function (url) {
    if (!url) return;
    var result = url.search(/([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i);
    if (result >= 0) {
      return url;
    } else {
      return false;
    }
  }
  Background.prototype.onYouTubeIframeAPIReady = function () {
    setTimeout(function () {
      this.setBg();
    }.bind(this),100)
  }

  Background.prototype.youtubeGetIdFromURL = function (picVideoUrl) {
    if (picVideoUrl.length < 13) return picVideoUrl;
    var youtubeId = picVideoUrl.split('v=')[1];
    var ampersandPosition = youtubeId.indexOf('&');
    if(ampersandPosition != -1) {
      youtubeId = youtubeId.substring(0, ampersandPosition);
    }
    return youtubeId;
  }
  return Background;
});
