/* global _*/
// 'use strict';



define([], function () {

  /********************* NOTI *********************/
  /********************* NOTI *********************/
  /********************* NOTI *********************/
  /********************* NOTI *********************/



  function Noti(parent) {
    this._xsb = parent;
    // this.el = $('.xsb_save')[0];
    // this.el = $('.xsb_noti_b')[0];
    this.color = 'saved';
  }

  Noti.prototype.showText = function (text, color, time) {
    return;

    time = time || 600;
    $('[data-attr=\'value\']')
    $('.xsb_noti').addClass('shown');
    $('.xsb_noti_b').addClass('shown');
    $('.xsb_noti_b')[0].innerText = text || 'Saved!';
    $('.xsb_noti_b').addClass(color || this.color);

    setTimeout(function () {
      $('.xsb_noti_b').removeClass('shown');
      $('.xsb_noti_b').removeClass(color || this.color);
    }.bind(this), time)

    setTimeout(function () {
      $('.xsb_noti').removeClass('shown');
    }.bind(this), time + 100)

  }





  return Noti;
})