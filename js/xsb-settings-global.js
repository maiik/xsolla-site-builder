/* global _*/
// 'use strict';



define([
  'js/xsb-colorpicker',
  'js/xsb-background',
], function (ColorPicker, Background) {



  function SettingsGlobal(parent, globName) {
    this._parent = parent;
    this._xsb = this._parent._xsb;
    this.globName = globName;
    this.triggers;
    this.colorPickers = {};
    this.backgroundMedia;
    this.languages = this.getLanguages();
    this.setListeners();
  }

  SettingsGlobal.prototype.getLanguages = function () {
    if (this.globName !== 'lang') return false;
    var thiss = this;
    var languages = {};

    $(':attr(\'^data-xsb-control-panel-global-lang-check\')').each(function (i, oneCheck) {
      var nam = oneCheck.dataset.xsbControlPanelGlobalLangCheck;
      languages[nam] = {};
      languages[nam]['check'] = oneCheck;
      languages[nam]['userSelector'] = $("[data-xsb-control-panel-global-language='" + nam + "']")[0];
      languages[nam]['userSelectorVisibility'] = thiss._xsb.dataX.global['languages'][nam];

      if (languages[nam]['userSelectorVisibility']) {
        languages[nam]['userSelector'].classList.remove('hidden');
      } else {
        languages[nam]['userSelector'].classList.add('hidden');
      }
      $(languages[nam]['check']).find('input:checkbox:first').prop('checked', languages[nam]['userSelectorVisibility']);

      $(languages[nam]['check']).on({
        click: function (evt) {
          if ($(languages[nam]['check']).find('input:checkbox:first').attr('checked', 'checked')[0].checked) {
            languages[nam]['userSelector'].classList.remove('hidden');
            thiss._xsb.dataX.global['languages'][nam] = true;
          } else {
            languages[nam]['userSelector'].classList.add('hidden');
            thiss._xsb.dataX.global['languages'][nam] = false;

            //если выключили язык, который активный, сбросить на дефолтный
            if (thiss._xsb.dataX.global['languages']['active'] === nam) {
              thiss._xsb.dataX.global['languages']['active'] = 'en_US';
              thiss._xsb.setData();
              thiss._xsb.changeLanguage('en_US');
            }
          }
          thiss._xsb.setData();
        }
      });

      $(languages[nam]['userSelector']).on({
        click: function (evt) {
          $(':attr(\'^data-xsb-control-panel-global-language\')').removeClass('active');
          this.classList.add('active');
          var newLang = this.dataset.xsbControlPanelGlobalLanguage;
          thiss._xsb.changeLanguage(newLang);
        }
      });

      return languages;

    });



    // $(':attr(\'^data-xsb-control-panel-global-language\')').on({
    //   click: function (evt) {
    //     $(':attr(\'^data-xsb-control-panel-global-language\')').removeClass('active');
    //     this.classList.add('active');
    //     var newLang = this.dataset.xsbControlPanelGlobalLanguage;
    //     thiss._xsb.changeLanguage(newLang);
    //   }
    // });
  }

  SettingsGlobal.prototype.onColorChange = function (pickerName, newColor) {
    this._xsb.setCustomTheme(pickerName, newColor);
  }


  SettingsGlobal.prototype.setListeners = function () {
    var thiss = this;


    var $form = $('[data-xsb-settings-global=\'' + this.globName + '\']');
    if (this.globName === 'colors') {
      //COLORS
      $form.find(':attr(\'^data-xsb-control-panel-global-color\')').each(function (i, item) {
        var pickerName = item.dataset.xsbControlPanelGlobalColor;
        thiss.colorPickers[pickerName] = new ColorPicker(thiss, item, thiss.onColorChange)
      });

      //ROUNDNESS CHNGE
      $(':attr(\'^data-xsb-control-panel-global-style\')').on({
        change: function (evt) {
          // $(':attr(\'^data-xsb-control-panel-global-scheme\')').removeClass('active');
          var newRadius = this.value;
          thiss._xsb.dataX['global']['userShapes']['roundness'] = newRadius / 30 + 'em';
          thiss._xsb.setData('global', thiss._xsb.dataX.global);
          thiss._xsb.changeTheme(thiss._xsb.dataX['global']['userShapes'], thiss._xsb.defaults.shape);
        }
      });

    }



    // if (this.globName === 'bg') {
    //   this.backgroundMedia = new Background(this);
    // }


  }


  return SettingsGlobal;
})