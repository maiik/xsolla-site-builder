/* global _*/
// 'use strict';



define([
], function () {



  function TemplateMediaController(_parent, newCommand, el) {
    this._parent = _parent;
    this.el = el;
    this._xsb = _parent._xsb;
    this.slider = this._parent.slider;
    this.newCommand = newCommand;
    this.addListeners();
  }


  TemplateMediaController.prototype.addListeners = function () {
    var thiss = this;
    $(this.el).on({
      click: function (evt) {
        thiss._parent[thiss.newCommand]();
      }
    })
  }



  return TemplateMediaController;
})