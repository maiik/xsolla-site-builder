/* global _*/
// 'use strict';



define([
  'js/xsb-template-media-controller',
], function (TemplateMediaController) {



  function TemplateMedia(_parent, mediaName, mediaNameUniq, el, controls, dataT) {
    this._parent = _parent;
    this._xsb = _parent._xsb;
    this.controls = controls || ['edit'];
    this.mediaName = mediaName;
    this.mediaNameUniq = mediaNameUniq;
    this.dataT = this.getData(dataT);
    this._parentName = this._parent.templateName || 'body';
    this.tint;
    this.el = this.getEl(el);
    this.cntrllrs = this.createControllers()
    // this.imgUrl = this.getImage();
    this.customStyle = this.getStyle();
    this.setImage();
    this.setStyle();
  }


  TemplateMedia.prototype.getData = function (dataT) {
    var newDataT = dataT || this.dataT;
    if (!newDataT) {
      newDataT = this._xsb.globalDefault['media'][this.mediaName]
      if (!newDataT) {
        newDataT = this._xsb.globalDefault['media']['defaultPic'];
      }
      this.dataT = newDataT;
    };
    return newDataT;
  }
  // TemplateMedia.prototype.getData = function () {
  //   var dat = this._parent.dataT[this._parent._mediaType][this.mediaName];
  //   if (!dat) {
  //     try {
  //       dat = this._xsb.modulesDefault[this._parent.templateName][this._parent._mediaType][this.mediaName];
  //     } catch (e) {
  //       console.log('e = ', e);
  //     }
  //   }
  //   if (!dat) {
  //     try {
  //       dat = this._xsb.globalDefault[this.mediaName];
  //     } catch (e) {
  //       console.log('e = ', e);
  //     }
  //   }
  //   return dat;
  // }

  TemplateMedia.prototype.getEl = function (el) {
    this.tint = document.createElement('DIV');
    this.tint.classList.add('xsb_bgz_tint');
    try {
      el.appendChild(this.tint);
    } catch (e) {
      console.log(this.mediaName) //TODO: wtf
    }
    return el;
  }




  TemplateMedia.prototype.getTint = function () {
    var newColor;
    try {
      newColor = this.dataT['tint'];
    } catch (e) {}
    if (!newColor) {
      newColor = (this.mediaNameUniq === 'bg') ? 'rgba(0, 0, 0, 0.49)' : 'rgba(0, 0, 0, 0.0)';
      this.setTint(newColor);
    }
    return newColor;
  }




  TemplateMedia.prototype.createControllers = function () {
    var ctrls = {};
    var thiss = this;
    var newContrParent = document.createElement('DIV');
    newContrParent.classList.add('xsb_template_media');
    if (thiss.mediaName === 'bg') newContrParent.classList.add('xsb_template_media--bg');

    //добавляем нужные контролы
    (this.controls.length) && this.controls.forEach(function (oneControllerName, i) {
      var newControl = document.createElement('DIV');
      newControl.classList.add('xsb_template_media_control');
      newControl.classList.add('xsb_template_media_control_' + oneControllerName);
      if (thiss.mediaName !== 'bg') {
        thiss.el.appendChild(newContrParent);
      } else {
        try {
          thiss._parent.el.appendChild(newContrParent);
        } catch (e) {}
      }
      newContrParent.appendChild(newControl);
      ctrls[oneControllerName] = new TemplateMediaController(thiss, oneControllerName, newControl);
    });

    //Находим в настройках статичные контроллеры
    var staticController = $("[xsb_template_media_control='" + this._parentName + "']")[0];
    if (staticController) ctrls['static'] = new TemplateMediaController(thiss, 'edit', staticController);

    return ctrls;
  }






  // TemplateMedia.prototype.getImage = function () {
  //   var newImg = this.dataT;
  //   if (!newImg) this.dataT = this._xsb.globalDefault['media'][this.mediaName];
  //   return this.dataT || this._xsb.globalDefault['media']['defaultPic'];
  // }





  TemplateMedia.prototype.setImage = function (oneMediaId, save) {
    // if (!this._xsb.dataX.global.media[oneMediaId]) return;

    if (!oneMediaId) {
      // oneMediaId = this.mediaName;
      // this.dataT = this._xsb.dataX.global.media[oneMediaId];
    } else {
      var newDataT = this._xsb.dataX.global.media[oneMediaId];
      if (!newDataT) newDataT = this._xsb.dataX.global.media['blank']
      this.dataT = newDataT;
    }
    if (save) {
      this.dataT = newDataT;
      var typ = this._parent._mediaType || 'media'; //TODO: разобраться с медиа
      if (this._parent.dataT[typ][this.mediaName][this.mediaNameUniq]) {
        this._parent.dataT[typ][this.mediaName][this.mediaNameUniq] = newDataT;
      } else {
        this._parent.dataT[typ][this.mediaName] = newDataT;
      }
      // this._parent.dataT['media'][this.mediaName][this.mediaNameUniq] = this.dataT;
      this._xsb.setData();
    }
    this.updateContent();
    this.setTint(this.getTint(), save);
  }



  TemplateMedia.prototype.updateContent = function (newDataT) {
    newDataT = newDataT || this.dataT;
    var thiss = this;
    // if (!newDataT) return;
    $(this.el).css('background-image', 'url(\'' + this.dataT['url'] + '\')');


    $(':attr(\'^data-xsb-settings-media-style\')').css({ 'background-image': 'url(\'' + this.dataT['url'] + '\')' });

    // if (thiss._parent.medias && thiss._parent.medias[thiss.mediaName]) {
    //   Object.keys(thiss._parent.medias[thiss.mediaName]).forEach(function (oneMediaNameUniq, i) {
    //     var mediaEl = thiss._parent.medias[thiss.mediaName][oneMediaNameUniq]['el'];
    //     $(mediaEl).css('background-image', 'url(\'' + newDataT['url'] + '\')');
    //   });
    // } else {
    //   $(this.el).css('background-image', 'url(\'' + newDataT['url'] + '\')');
    // }
  }






  TemplateMedia.prototype.delete = function () {
    this.el.parentElement.removeChild(this.el);
    this._parent.deleteMedia(this.mediaName, this.el);
  }


  TemplateMedia.prototype.edit = function () {
    this._xsb.settngs.setActive('media');
    this._xsb.settngs.mediaSettings.setTarget(this);
    var thiss = this;
    var $styleRadios = $(':attr(\'^data-xsb-control-panel-module-style\')');
    $styleRadios.each(function (i, oneRadio) {
      var newStyle = oneRadio.dataset.xsbControlPanelModuleStyle.split(',');
      var newStyleName = newStyle[0];
      var newStyleVal = newStyle[1];
      if (thiss.customStyle[newStyleName] === newStyleVal) {
        $('[data-xsb-control-panel-module-style=\'' + newStyleName + ',' + newStyleVal + '\']').find('input:checkbox:first').prop('checked', true);
        $('[data-xsb-control-panel-module-style=\'' + newStyleName + ',' + newStyleVal + '\']').find('input:radio:first').prop('checked', true);
      }
    })

    // var newImage = this._xsb.settigs.setMedia(this);
  }






  TemplateMedia.prototype.getStyle = function () {
    var newCSS = {};

    if (!this.dataT) this.dataT = {};
    if (!this.dataT)
      this.dataT = {};
    if (!this.dataT)
      this.dataT = {};

    if (this.dataT['style']) {
      newCSS = this.dataT['style'];
    } else {
      // imgUrl = this._xsb.globalDefault['media']['defaultPic'];
    }
    return newCSS;
  }

  TemplateMedia.prototype.setStyle = function () {
    if (!Object.keys(this.customStyle).length) return;
    var thiss = this;
    Object.keys(thiss.customStyle).forEach(function (oneStyleName, i) {
      var oneStyleVal = thiss.customStyle[oneStyleName];
      thiss.updateStyle(oneStyleName, oneStyleVal, 'save');
    })
  }


  TemplateMedia.prototype.updateStyle = function (newStyleName, newStyleVal, save) {
    var thiss = this;
    if (!(newStyleName && newStyleVal)) return;
    if (save) {
      this.customStyle[newStyleName] = newStyleVal;
      this.dataT['style'] = this.customStyle;
      this._xsb.setData();
    }

    if (thiss._parent.medias) {
      Object.keys(thiss._parent.medias[thiss.mediaName]).forEach(function (oneMediaNameUniq, i) {
        var mediaEl = thiss._parent.medias[thiss.mediaName][oneMediaNameUniq].el
        $(mediaEl).css(newStyleName, newStyleVal);
      });
    } else {
      $(this.el).css(newStyleName, newStyleVal);
    }
  }







  TemplateMedia.prototype.setTint = function (newColor, save) {

    if (!this.dataT) this.dataT = {};
    if (!this.dataT['tint']) this.dataT = this.getData();

    if (!newColor) {
      newColor = this.dataT['tint'];
    }
    if (newColor) {
      this.dataT['tint'] = newColor;
    }
    if (save) {
      this._xsb.setData();
    }
    $(this.tint).css('background-color', this.dataT['tint']);

    // if (thiss._parent.medias && thiss._parent.medias[thiss.mediaName]) {
    //   Object.keys(thiss._parent.medias[thiss.mediaName]).forEach(function (oneMediaNameUniq, i) {
    //     var mediaTint = thiss._parent.medias[thiss.mediaName][oneMediaNameUniq]['tint']
    //     $(mediaTint).css('background-color', newColor);
    //   });
    // } else {
    //   $(this.tint).css('background-color', newColor);
    // }
  }





  return TemplateMedia;
})