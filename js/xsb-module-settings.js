/* global _*/
// 'use strict';



define([
  'js/xsb-module-settings-variants',
], function (ModuleVariantController) {



  /********************* MODULE SETTINGS *********************/
  /********************* MODULE SETTINGS *********************/
  /********************* MODULE SETTINGS *********************/
  /********************* MODULE SETTINGS *********************/



  function ModuleSettings(parent, contName) {
    this._parent = parent;
    this._xsb = this._parent._xsb;
    this.contName = contName;
    this._template = this.getTemplate();
    // this.dataModule = this._xsb.dataX['modules'];
    this.el = this.getModuleSettings();
    // this.allSettings = this.getAllSettings();
  }

  ModuleSettings.prototype.getModuleSettings = function () {
    return $("[data-xsb-control-module='" + this.contName + "']")[0];
  }


  ModuleSettings.prototype.getTemplate = function () {
    return this._xsb.templates[this.contName];
  }

  ModuleSettings.prototype.showHide = function (doo) {
    if (doo === 'show' && this.el) {
      this.el.classList.remove('hidden');
    }1
  }

  ModuleSettings.prototype.getAllSettings = function () {
    return;
    var allSett = {};
    var thiss = this;
    $(this.el).find(':attr(\'^data-xsb-settings-module\')').each(function (i, oneSett) {
      var settType = oneSett.dataset.xsbSettingsModule;
      var callback = thiss._template.setActiveVar;
      var callbackScope = thiss._template;
      switch (settType) {
        case 'variant':
          allSett[settType] = new ModuleVariantController(thiss, thiss.contName, oneSett, callback, callbackScope); //TODO: разобраться с bind / call или сделать правильно
          break;
      }
    });
    return allSett;
  }



  return ModuleSettings;
})