/* global _*/
// 'use strict';



define([
  'js/xsb-module-settings',
], function (ModuleSettings) {



  /********************* CONTROLLER *********************/
  /********************* CONTROLLER *********************/
  /********************* CONTROLLER *********************/
  /********************* CONTROLLER *********************/



  function Controllr(parent, contName) {
    this._xsb = parent;
    this.contName = contName;
    this.dataModule = this._xsb.dataX['modules'];
    this.dataC = this.getData();
    this.el = this.getController(this.contName);
    this.template = this.getTemplate();
    this.active = this.getActive();
    this.setActive(this.active);
    this.addSettings = this.getAddSettings();
    this.addListener();
  }




  Controllr.prototype.getData = function () {
    // if (!this.dataModule[this.contName]) {

    // }
    return this.dataModule[this.contName];
  }


  Controllr.prototype.getTemplate = function () {
    // return new Template(this, this.contName);
    return this._xsb.templates[this.contName];
  }




  Controllr.prototype.getActive = function () {
    if (this.dataC.shown) {
      this.active = true;
    } else {
      this.active = false;
    }
    return this.active
  }




  Controllr.prototype.setActive = function (state) {
    this.active = typeof state !== 'undefined' ? state : this.getActive();
    // this._xsb.dataX['modules'][this.contName].shown = state;
    this.dataC.shown = this.active;
    this.updateState();
    this._xsb.setData();
        //Take care of template
    // this.template.shown = this.active;
    if (this.template) this.template.updateState();
  }


  Controllr.prototype.updateState = function () {
    if (this.getActive()) {
      $(this.el).addClass('module_active');
      $(this.el).find('.sb_1m_contol_on').removeClass('hidden');
      $(this.el).find('.sb_1m_contol_off').addClass('hidden');
      $(this.el).find('.sb_1m_contol_sett').removeClass('hidden');
    } else {
      $(this.el).removeClass('module_active');
      $(this.el).find('.sb_1m_contol_on').addClass('hidden');
      $(this.el).find('.sb_1m_contol_off').removeClass('hidden');
      $(this.el).find('.sb_1m_contol_sett').addClass('hidden');
    }
  }


  Controllr.prototype.getAddSettings = function () {
    return new ModuleSettings(this, this.contName);
    // return $("[data-xsb-control-module='" + this.contName + "']")[0];
  }


  Controllr.prototype.addListener = function () {
    var thiss = this;
    $(this.el).find('.sb_1m_contol_onoff').on({
      click: function (evt) {
        thiss.setActive(!thiss.active);
        thiss.template.updateState();
        // thiss.template.moduleHighlight(thiss.template);
        evt.stopPropagation();
      }
    });


    // $('.xsb_sett').addClass('shown');
    // $('.xsb_sett_b, .xsb_z').removeClass('shown');
    setTimeout(function () {
      $('.xsb_sett').removeClass('shown');
    }, 200);
    // if (this.contName !== 'widgets' && this.contName !== 'bg') {
      $('[data-xsb-control-module=\'' + this.contName + '\']').addClass('hidden');
    // }



    //Module Settings
    $(this.el).find(':attr(\'^data-xsb-control-edit\')').on({
    // $(this.el).find('.sb_1m_module_bghover').on({
    // $(this.el).on({
      click: function (evt) {

        if (!$(this).closest('.sb_1m_module').is('.module_active')) {
          thiss.active = true;
          thiss.setActive(true);
        };
        thiss.template.moduleHighlight();
        thiss.showHideAdditionalSettings()

        // $('.xsb0').removeClass('shown');
        // thiss.showHideControls();
        evt.stopPropagation();
      }
    });


    // $(this.el).find('.sb_1m_module_title').on({
    //   click: function (evt) {
    //     var toScroll = this.closest('.sb_1m_module').dataset.xsbControlAdd;
    //     var scrollToEl = '[data-xsb-template-type=\'' + toScroll + '\']';
    //     // thiss.moduleHighlight($(scrollToEl)[0]);
    //     thiss.template.moduleHighlight(thiss.template.el)
    //     evt.stopPropagation();
    //   }
    // });
    //


    //OPEN Settings
    $(this.el).on({
      click: function (evt) {
        // var toScroll = this.closest('.sb_1m_module').dataset.xsbControlAdd;
        // var scrollToEl = '[data-xsb-template-type=\'' + toScroll + '\']';
        // thiss.moduleHighlight($(scrollToEl)[0]);
        // thiss.settings.setShown();

        // thiss.template.moduleHighlight();
        // $('.xsb_sett').addClass('shown');
        // $('.sb_1modules').addClass('moved');
      }
    });


  }



  Controllr.prototype.getController = function (name) {
    return $("[data-xsb-control-add='" + name + "']")[0]
  }




  Controllr.prototype.showHideAdditionalSettings = function () {

    //Show add settings
    $('.xsb_sett').addClass('shown');
    setTimeout(function () {
      $('.xsb_sett_b, .xsb_z').addClass('shown');
    }, 1);

    //Show add settings
    // $('[data-xsb-settings-panel=\'modules\']').find('.xsb_settings_inner').addClass('moved');
    // $('.xsb0').addClass('moved'); //TODO: это сдвигание влево главной панели
    // $('.sb_1m_module').removeClass('highlighted');
    // $(this.el).addClass('highlighted');
    // $(':attr(\'^data-xsb-control-module\')').addClass('hidden');




    //Show add settings
    $('[data-xpop=\'submenu\']').addClass('moved');
    $('[data-xpop=\'submenu\']').find('[data-xpop=\'b\']').addClass('moved');
    $('[data-xpop=\'submenu\']').find('[data-xpop=\'z\']').addClass('moved');
    // $('.xsb0').addClass('moved'); //TODO: это сдвигание влево главной панели
    $('.sb_1m_module').removeClass('highlighted');
    $(this.el).addClass('highlighted');
    $(':attr(\'^data-xsb-control-module\')').addClass('hidden');




    // if (this.contName !== 'widgets') {
      // $('[data-xsb-control-module=\'' + this.contName + '\']').addClass('hidden'); //TODO: getacрятать по-умному

    // }
    // if (this.contName !== 'bg') {
    //   $('[data-xsb-control-module=\'' + this.contName + '\']').addClass('hidden'); //TODO: getacрятать по-умному
    // }
    this.addSettings.showHide('show');
  }



  return Controllr;
})