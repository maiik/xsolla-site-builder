/* global _*/
// 'use strict';



/**
 * Вся панель настроек, показ скрытие панели
 */


define([
  'js/xsb-settings-global',
  'js/xsb-settings-panel',
  'js/xsb-settings-media',
], function (SettingsGlobal, SettingsPanel, SettingsMedia) {



  function Settings(parent, deflt) {
    this._xsb = parent;
    this.c = {};
    this.c['$panel'] = $('.xsb');
    this.c['$panelControls'] = $(':attr(\'^data-xsb-control-panel-appearance\')');
    this.active = deflt || 'modules'; //Default module shown
    this.panels = this.getModuleSettings();
    this.setActive(this.active);
    this.shown = this.setShown(this._xsb.dataX['global']['panel']);
    this.setInitials()
    this.globalSettings = this.getGlobalSettings();
    this.mediaSettings = this.getMediaSettings();
    this.localize();
    this.disableWebflowForms();
    this.addBack2PA()
  }


  Settings.prototype.addBack2PA = function () {
    $('.xsb_logocont').on({
      // click: function (evt) {
      //   $(':attr(\'^data-xsb-control-panel-global-language\')').removeClass('active');
      //   this.classList.add('active');
      //   var newLang = this.dataset.xsbControlPanelGlobalLanguage;
      //   thiss._xsb.changeLanguage(newLang);
      // },
      mouseover: function (evt) {
        $(this).find('.xsb_0_li_hint').addClass('shown');
        $(this).find('.xsb_back2pa').addClass('shown');

      },
      mouseout: function (evt) {
        $(this).find('.xsb_0_li_hint').removeClass('shown');
        $(this).find('.xsb_back2pa').removeClass('shown');
      },
    });
  }



  Settings.prototype.disableWebflowForms = function () {
    var thiss = this;
    Webflow.push(function() {
      // Disable submitting form fields during development
      thiss.c['$panel'].find('form').submit(function(e){
        // console.log('Form submissions have been disabled during development.');
        return false;
      });
    });
  }


  Settings.prototype.localize = function () {
    var thiss = this;
    var $els;
    $els = $(':attr(\'^data-xsb-localization\')');
    $els.each(function (i, item) {
      var key = item.dataset.xsbLocalization;
      var vl = thiss._xsb.defaults.localization[thiss._xsb.local][key];
      if (vl) item.textContent = thiss._xsb.defaults.localization[thiss._xsb.local][key];
    });
  }

  /**
   * Собирает панели глобальных настроек
   */
  Settings.prototype.getGlobalSettings = function () {
    var thiss = this;
    var globalSettings = {};
    var globals = $(':attrStrict(\'data-xsb-settings-global\')');
    globals.each(function (i, globEl) {
      var globName = globEl.dataset.xsbSettingsGlobal;
      var oneGlobal = new SettingsGlobal(thiss, globName);
      globalSettings[globName] = oneGlobal;
    })
    return globalSettings;
  }



  /**
   * Собирает медиа-настройки
   */
  Settings.prototype.getMediaSettings = function () {
    var mediaSettEl = $('[data-xsb-settings=\'media\']')[0];
    return new SettingsMedia(this, mediaSettEl);
  }



  /**
   * Собирает настройки темплейтов
   */
  Settings.prototype.getModuleSettings = function () {
    var panels = $(':attrStrict(\'data-xsb-settings-panel\')');
    var thiss = this;
    var panelsObj = {};
    panels.each(function (i, panelEl) {
      var panelName = panelEl.dataset.xsbSettingsPanel;
      var active = false;
      if (panelName === thiss.active) {
        active = true;
      }
      var onePanel = new SettingsPanel(thiss, panelName);
      panelsObj[panelName] = onePanel;
    })
    return panelsObj;
  }




  Settings.prototype.setActive = function (activeName) {

    if (activeName === 'media') { //TODO: показ медии переделать
      $('[data-xpop=\'media\']').addClass('shown');
      $('[data-xpop=\'media\']').find('[data-xpop=\'b\']').addClass('shown');
      $('[data-xpop=\'media\']').find('[data-xpop=\'z\']').addClass('shown');
      return;
    };

    var thiss = this;

    Object.keys(this.panels).forEach(function (panelName) {
      var p = thiss.panels[panelName];
      var pAct = thiss.panels[activeName];
      if (panelName === activeName) {
        p.showPanel();
        if (p.triggerType === 'toggle') {
          thiss.panels[panelName].showResult();
          p.showTriggerActive();
          location.hash = activeName;
        }
      } else {
        p.hidePanel();
        if (p.triggerType === 'toggle' && pAct.triggerType === 'toggle') {
          thiss.panels[panelName].hideResult()
          p.hideTriggerActive();
        }
      }
    });
    this.setShown('show')
  }


  Settings.prototype.setInitials = function (activeName) {
    var thiss = this;

    //LANGUAGE
    (function () {
      $(':attr(\'^data-xsb-control-panel-global-language\')').removeClass('active');
      $("[data-xsb-control-panel-global-language='" + thiss._xsb.dataX['global']['languages']['active'] + "']").addClass('active');
    })();



    //LOCALSTORAGE CLEAR
    $('#tools_localstorageclear').on({
      click: function (evt) {
        localStorage.clear();
        window.location.reload(true);
      }
    });


    //ESC PANEL
    $(document).keyup(function(e) {
      if (e.keyCode == 27) { // escape key maps to keycode `27`
        thiss.setShown('toggle')
      }
    });


    this.c['$panelControls'].on({
      click: function (evt) {
        if (this.dataset.xsbControlPanelAppearance) {
          thiss.setShown(this.dataset.xsbControlPanelAppearance);
          return;
        }
      }
    });


  }





  Settings.prototype.setShown = function (shown) {
    var doo;
    if (shown === 'toggle') {
      shown = !this._xsb.dataX['global']['panel'];
    }
    if (shown === 'show' || (typeof shown === 'boolean' && shown)) {
      doo = 'show';;
    }
    if (!shown || shown === 'hide') {
      doo = 'hide';
      shown = false;
    }
    if (shown === 'back') {
      shown = true;
      this.setActive(this.active);
    }


    if (shown === 'move') {
      doo = 'move';
      shown = true;
    }


    this._xsb.dataX['global']['panel'] = shown;
    this.xbPanel(doo);
    this._xsb.setData();
  }



  Settings.prototype.xbPanel = function (doo) { //TODO: тут все плохо
    if (doo === 'show') {
      this.c['$panel'].addClass('shown');
      // this.c['$panel0'].addClass('shown');
      // this.c['$xsbCall'].removeClass('shown');
      $('.xsb_container').addClass('moved');
      $('.xsb_body_bg').addClass('moved');
    }
    if (doo === 'hide') {
      this.c['$panel'].removeClass('shown');
      // this.c['$panel0'].removeClass('shown');
      // this.c['$xsbCall'].addClass('shown');
      $('.xsb_container').removeClass('moved');
      $('.xsb_body_bg').removeClass('moved');
    }
    if (doo === 'toggle') {
      // this.c['$panel'].toggleClass('shown');
      // this.c['$panel0'].toggleClass('shown');
      // this.c['$xsbCall'].toggleClass('shown');
      $('.xsb_container').toggleClass('moved');
      $('.xsb_body_bg').toggleClass('moved');
    }

    if (!this._xsb.dataX['global']['panel']) {
      $('.xsb_0_icon_sprite').removeClass('moved');
    }
  }





  return Settings;
})