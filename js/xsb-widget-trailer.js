/* global _*/
// 'use strict';



define([
  'swiper',
  // 'require_css!swiper_css',
  // 'require_css!swiper_css_mike'
], function (
  Swiper,
  // requirecss,
  // requirecssmike
) {



  function WidgetTrailer(_xsb) {
    this._xsb = _xsb;
    // console.log('hyi = ', 'hyi');


    this.swiperPop = new Swiper('.swiper-pop-0', {
      //containerModifierClass: 'swiper-pop',
      spaceBetween: '5%',
      centeredSlides: true,
      slidesPerView: 'auto',
      centerInsufficientSlides: true,
      //slideClass: 'swiper-pop',
      //loop: true,
      //setWrapperSize: true,
      //preventClicks: false,
      //preventClicksPropagation: false,
      //'wrapperClass' : 'swiper-wrapper-pop',
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },
      navigation: {
        nextEl: '.swiper-button-next-m',
        prevEl: '.swiper-button-prev-m',
      },
    });

    this.swiperPopThumb = new Swiper('.swiper-pop-thumb-0', {
        //containerModifierClass: 'swiper-pop-thumb',
        //spaceBetween: 10,
        //slideClass: 'swiper-pop-thumb',
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true,
        //'wrapperClass' : 'swiper-wrapper-pop',
        //preventClicks: false,
        //preventClicksPropagation: false,
    });

    this.swiperPopThumb.controller.control = this.swiperPop;
    this.swiperPop.controller.control = this.swiperPopThumb;



    // this.swiperPop.on('slideChange', function () {
    //     player.stopVideo();
    //   });


  }





  return WidgetTrailer;
})