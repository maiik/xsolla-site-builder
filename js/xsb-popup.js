/* global _*/
// 'use strict';



define([

], function () {


  function Popup(parent, popName) {
    this.parent = parent;
    this.popName = popName;
    this.$target = this.getTarget();
    this.$triggerEl = this.getEl();
    this.addListeners();
  }


  Popup.prototype.getEl = function () {
    return $('[data-xsb_popup=\'' + this.popName + '\']');
  }

  Popup.prototype.getTarget = function (targetName) {
    return $('[data-xsb_popup_target=\'' + this.popName + '\']');
  }

  Popup.prototype.addListeners = function (targetName) {
    var thiss = this;
    this.$triggerEl.on({
      click: function (evt) {
        $('.xsb_pop').addClass('shown');
        thiss.$target.addClass('shown');
      }
    });

    this.$target.parent().find(':attr(\'^data-xsb_popup_close\')').on({
      click: function (evt) {
        thiss.$target.removeClass('shown');
        $('.xsb_pop').removeClass('shown');
      }
    });


  }



  return Popup;
})