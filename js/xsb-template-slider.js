/* global _*/
// 'use strict';



define([
  'https://xsolla-sitebuilder.netlify.com/js/xsb-template-media.js',
], function (TemplateMedia) {



  function TemplateSlider(template, sliderName, el) {
    this._template = template;
    this._mediaType = 'slider';
    this._sliderName = sliderName;
    this._xsb = this._template._xsb;
    this.el = el;
    this.dataT = this._xsb.dataX['modules'][this._template.templateName]['gallery'][sliderName];
    this.templateName = this._template.templateName;
    this.activeIndex = 0;
    this.cntrllers = {};
    this.sliderSettings = this.getSliderSettings();
    // this.dataT = this.getSliderData();
    // this.dataT = this._template.dataT;
    this.medias = {};
    this.slider;

    this.updateSlider();
    this.addListeners();
  }



  TemplateSlider.prototype.getSliderSettings = function () {
    var defaultSlider = {
      loop: false,
      centeredSlides: true,
      // setWrapperSize: true,
      slidesPerView: 'auto',
      activeIndex: 0,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
      },
      on: {
        init: function () {
        },
      },
      noSwipingClass: 'xsb_template_media_control',
    }

    var specific = this.dataT;
    if (!specific) return defaultSlider;
    Object.keys(specific).forEach(function (oneKey, i) {
      var oneSpec = specific[oneKey];
      defaultSlider[oneKey] = specific[oneKey];
    });
    return defaultSlider;
  }




  TemplateSlider.prototype.getSliderData = function () {
    if (this._template.dataT) {
      return this._template.dataT;
    }
    return { 'media': null };
  }




  TemplateSlider.prototype.addListeners = function () {
    var $sliderContr = $(this.el).find(':attr(\'^data-xsb-template-controller\')');
    var thiss = this;
    $sliderContr.each(function (i, sliderContr) {
      var newCommand = sliderContr.dataset.xsbTemplateController;
      // thiss[newCommand] = new TemplateSliderController(thiss, newCommand, sliderContr)

      $(sliderContr).on({
        click: function (evt) {
          thiss[newCommand]();
        }
      });

    })

  }




  TemplateSlider.prototype.updateSlider = function () {
    var slidesArr = Object.keys(this.dataT);
    this.swiperWrapper = this.el.querySelector('.swiper-wrapper');
    // this.swiperWrapper.innerHTML = '';
    var thiss = this;
    if (this.swiperWrapper.childNodes.length) {
      thiss.swiperWrapper.innerHTML = '';
    }

    slidesArr.forEach(function (oneSlideId, i) {

      var newEl = document.createElement('div');
      newEl.classList.add('swiper-slide');
      newEl.classList.add(thiss._sliderName);
      thiss.swiperWrapper.appendChild(newEl);
      thiss.medias[oneSlideId] = {};
      var newDataT = thiss.dataT || thiss._xsb.globalDefault.media.sliderBlank;
      thiss.medias[oneSlideId][oneSlideId] = new TemplateMedia(thiss._template, oneSlideId, oneSlideId, newEl, ['edit', 'delete'], newDataT);
    })
    // this.swiperWrapper.prepend(addNewSlide);

    this.recreate()
  }



  TemplateSlider.prototype.create = function () {
    this.activeIndex = Object.keys(this.dataT).length + 1;
    var newName = 'slide' + this.activeIndex;
    this._template.dataT[newName] = this._xsb.globalDefault['media']['slideDefault'];
    this.sliderSettings.activeIndex = this.activeIndex;
    this._xsb.setData();
    this.updateSlider();
  }




  TemplateSlider.prototype.deleteMedia = function (sliderName, el) {
    delete this.dataT;
    this.activeIndex = 0;
    this.sliderSettings.activeIndex = this.activeIndex;
    this._xsb.setData();
    this.updateSlider();
  }





  TemplateSlider.prototype.recreate = function () {
    if (!this.slider) {
      this.slider = this.createSlider();
      return;
    }
    // this.slider.params.slidesPerView = 2; //TEMP
    this.slider.update()
    // this.slider.pagination.update();
    this.slider.slideTo(this.activeIndex);
  }




  TemplateSlider.prototype.createSlider = function () {
    var thiss = this;
    function createSwiper() {
      setTimeout(function () {
        return new Swiper(thiss.el, thiss.sliderSettings)
      }, 100);
    };
    return createSwiper();
  }

  return TemplateSlider;
})