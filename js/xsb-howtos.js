/* global _*/
// 'use strict';



requirejs.config({
  baseUrl:  baseURL || 'https://xsolla-sitebuilder.netlify.com/',
  paths: {
    'quill':                          'https://cdn.quilljs.com/1.3.1/quill',
    '_':                              'https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min',
    'xsolla_genericpopups':           'https://cdn.xsolla.net/misc/mikelibs/genericpopups/latest/genericpopups',
    'swiper':                         'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min',
    'xsb-howtos-button':              'https://xsolla-sitebuilder.netlify.com/js/xsb-howtos-button',
  },
});


define([
  'xsb-howtos-button',
], function (HowTosButton) {



  function HowTos(_xsb) {
    this._xsb = _xsb;
    // console.log('hyi = ', 'hyi');
    this.swiperHow;
    this.swiperCont;
    this.howTosData = [
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/JYeLtMvgyPg',
        'title': 'How to change colors',
        'new': true,
        'highlighted': true,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/3gPsLA-dwNM',
        'title': 'Set up background',
        'new': true,
        'highlighted': false,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/JYeLtMvgyPg',
        'title': 'Set up Gallery',
        'new': true,
        'highlighted': false,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/JYeLtMvgyPg',
        'title': 'SEO Settings',
        'new': true,
        'highlighted': false,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/3gPsLA-dwNM',
        'title': 'Adding Analytics',
        'new': true,
        'highlighted': true,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/JYeLtMvgyPg',
        'title': 'Own domain',
        'new': true,
        'highlighted': false,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/JYeLtMvgyPg',
        'title': 'Tweaking an Image',
        'new': true,
        'highlighted': false,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/3gPsLA-dwNM',
        'title': 'Video Recomendations',
        'new': true,
        'highlighted': false,
      },
      {
        // 'id': '',
        'video': 'https://www.youtube.com/embed/JYeLtMvgyPg',
        'title': 'Clear image',
        'new': true,
        'highlighted': false,
      },
    ];
    this.videoSwipers = null;
    this.howTosButtons = [];
    this.addListeners();
  }


  HowTos.prototype.addListeners = function () {
    var thiss = this;
    $('#xsb_how_open').on({
      click: function (evt) {
        $(this).removeClass('shown');
        $('.xsb_how_cont').removeClass('hidden');
        setTimeout(function () {
          $('.xsb_how_cont').addClass('shown');
          if(!thiss.videoSwipers) {
            thiss.videoSwipers = thiss.createVideoSwipers();
          }
        }, 70);
      }
    });
    $('#xsb_how_close').on({
      click: function (evt) {
        $('.xsb_how_cont').removeClass('shown');
        setTimeout(function () {
          $('.xsb_how_cont').addClass('hidden');
          $('#xsb_how_open').addClass('shown');
        }, 100);
      }
    });
  }



  HowTos.prototype.createVideoSwipers = function () {
    var thiss = this;
    var swiperContClassName = '#xsb_howtos';
    var $swiperCont = $(swiperContClassName);
    var $swiperWrapper = $(swiperContClassName).find('.swiper-wrapper__howtos');
    // var $swiperSlide = $(swiperContClassName).find('.swiper-slide__howtos');
    var buttonCont = $('.xsb_pop_htcontents')[0];
    $swiperWrapper[0].innerHTML = '';
    this.howTosData.forEach(function (value, i) {
      var oneVideo = value['video'];
      var oneTitle = value['title'];
      var oneNew = value['new'];

      var newSlide = document.createElement('DIV');
      newSlide.classList.add('swiper-slide__howtos');
      // newSlide.textContent = i;
      // var embd = '<iframe class="xsb_help_video" width="360" height="200" src="https://www.youtube.com/embed/JYeLtMvgyPg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
      var embd = document.createElement('iframe');
      embd.classList.add('xsb_help_video');
      embd.src = oneVideo;
      // newSlide.innerHTML = embd;
      // $(newSlide).find('.xsb_help_video')[0].src = oneVideo;
      $swiperWrapper[0].appendChild(newSlide);
      newSlide.appendChild(embd);
    });


    var swiper = new Swiper('#xsb_howtos', {
      slidesPerView : 1,
      direction: 'horizontal',
      //loop: true, //no loop in controlled mode
      // centeredSlides: true,
      setWrapperSize: true,
      wrapperClass: 'swiper-wrapper__howtos',
      slideClass: 'swiper-slide__howtos',
    });

    buttonCont.innerHTML = '';

    this.howTosData.forEach(function (value, i) {
      var oneVideo = value['video'];
      var oneTitle = value['title'];
      var oneNew = value['new'];

      // var newButton = document.createElement('DIV');
      thiss.howTosButtons.push(new HowTosButton(thiss, buttonCont, i));
    });

    return swiper;
  }





  return HowTos;
})