/* global _*/
// 'use strict';



/**
 * Одна панель настроек
 */



define([

], function () {

  function SettingsPanel(parent, panelName) {
    this._xsb = parent._xsb;
    this._parent = parent;
    this.panelName = panelName;
    this.el = this.getPanel();
    this.$elTarget = this.getTarget();
    this.$trigger = this.getTrigger();
    this.triggerType;
    // this.setActive(this._parent.active);
    this.addTriggers();
    // this.modulesSettings = this.getModules();
  }






  SettingsPanel.prototype.getPanel = function () {
    var panel = $("[data-xsb-settings-panel='" + this.panelName + "']")[0];
    return panel;
  }

  SettingsPanel.prototype.getTrigger = function () {
    var $trigg = $("[data-xsb-settings-panel-trigger='" + this.panelName + "']");

    // if ($trigg.data()['xsbSettingsPanelTrigger'] === 'global') {

    switch (this.panelName) {
      case 'global':
        this.triggerType = 'popup';
        break;
      case 'media':
        this.triggerType = 'popup';
        break;

      default:
        this.triggerType = 'toggle';
        break;
    }

    $trigg = $trigg ||  $("[data-xsb-settings-global-trigger='" + this.panelName + "']");

    if (!$trigg) return;
    $trigg.removeClass('disabled');
    return $trigg;
  }

  SettingsPanel.prototype.getTarget = function () {
    var $target = $('[data-xsb-result=\'' + this.panelName + '\']');
    if ($target) return $target;
  }

  SettingsPanel.prototype.getModules = function () {
    if (this.panelName !== 'modules') return false;
    this.modulesSettings = this.modulesSettings || {};
  }




  SettingsPanel.prototype.showPanel = function () {
    this.el.classList.remove('hidden');
    this.$trigger.addClass('active');
    // if (this.elTarget) this.elTarget.classList.remove('hidden');
  }


  SettingsPanel.prototype.hidePanel = function () {
    // $(this.trigger).find('.xsb_0_icon_sprite').removeClass('moved');
    this.el.classList.add('hidden');
    // if (this.elTarget) this.elTarget.classList.add('hidden');
  }



  SettingsPanel.prototype.showResult = function () {
    if (this.$elTarget) this.$elTarget.removeClass('hidden');
  }
  SettingsPanel.prototype.hideResult = function () {
    if (this.$elTarget) this.$elTarget.addClass('hidden');
  }



  SettingsPanel.prototype.showTriggerActive = function () {
    if (this.$trigger) this.$trigger.addClass('active');
    if (this._xsb.dataX['global']['panel']) {
      this.$trigger.find('.xsb_0_icon_sprite').addClass('moved'); //TODO: доделать стрелочку
    }
  }
  SettingsPanel.prototype.hideTriggerActive = function () {
    if (this.$trigger) this.$trigger.removeClass('active');
    if (this._xsb.dataX['global']['panel']) {
      this.$trigger.find('.xsb_0_icon_sprite').removeClass('moved'); //TODO: доделать стрелочку
    }
  }








  SettingsPanel.prototype.addTriggers = function () {

    var thiss = this;
    var thissXsb = this._xsb;

    //ACTIVATE
    this.$trigger.on({
      click: function (evt) {
        evt.stopPropagation();

        if (thiss.triggerType !== 'popup') {
          if (this.dataset.xsbSettingsPanelTrigger === 'media') {
            thiss._xsb.settngs.mediaSettings.setTarget(null); //Убираем с панели медии настройки, кроме картинки
          }
          thiss._parent.setActive(thiss.panelName);
        } else {
          thiss._parent.setActive(thiss.panelName);
        }
      },
      mouseover: function (evt) {
        $(this).find('.xsb_0_li_hint').addClass('shown');
      },
      mouseout: function (evt) {
        $(this).find('.xsb_0_li_hint').removeClass('shown');
      }
    });

  }








  return SettingsPanel;
})