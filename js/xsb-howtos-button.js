/* global _*/
// 'use strict';



define([

], function () {

  function HowTosButton(_parent, parentEl, index) {
    var thiss = this;
    this._parent = _parent;
    this.parentEl = parentEl;
    this.index = index;
    this.buttonEl = document.createElement('DIV');
    this.buttonEl.classList.add('xsb_pop__hintcontentbutton');
    this.buttonEl.textContent = this._parent.howTosData[this.index]['title'];
    this.parentEl.appendChild(this.buttonEl);
    if (this.index === 0) {
      $(this.buttonEl).addClass('active');
      $('.xsb_menu_h')[0].textContent = thiss._parent.howTosData[thiss.index]['title'];
    }
    if (this._parent.howTosData[this.index]['highlighted']) {
      var indicator = document.createElement('DIV');
      indicator.classList.add('xsb_how_indicator');
      var indicatorInner = document.createElement('DIV');
      indicatorInner.classList.add('xsb_indicator');
      indicatorInner.classList.add('xsb_indicator--new');
      indicator.appendChild(indicatorInner);
      this.buttonEl.appendChild(indicator);
    }

    $(this.buttonEl).on({
      click: function (evt) {
        $(thiss.parentEl).find('.active').removeClass('active');
        $(this).addClass('active');
        $('.xsb_menu_h')[0].textContent = thiss._parent.howTosData[thiss.index]['title'];
        thiss._parent.videoSwipers.slideTo(thiss.index);
      }
    });
  }

  return HowTosButton;
})