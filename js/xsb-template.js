/* global _*/
// 'use strict';



define([
  'js/xsb-template-editor',
  'https://xsolla-sitebuilder.netlify.com/js/xsb-template-media.js',
  'js/xsb-template-slider',
  'js/xsb-template-controller-onoff',
], function (TemplateEditor, TemplateMedia, TemplateSlider, TemplateControllerOnOff) {




  /********************* TEMPLATE *********************/
  /********************* TEMPLATE *********************/
  /********************* TEMPLATE *********************/
  /********************* TEMPLATE *********************/




  function Template(parent, name) {
    this._xsb = parent._xsb;
    this.controller = this._xsb.templateControllers[name] || null;
    this.templateName = name;
    this._mediaType = 'media';
    this.dataT = this._xsb.dataX['modules'][name];
    this.el = this.getTemplate(this.templateName);
    // this.elVars = this.getVars();
    this.shown = this.getShown();
    this.gallery;
    this.medias = this.gatherMedia();
    this.sliders = this.gatherSliders();
    this.editors = this.gatherEditors();
    this.onoffs = this.gatherOnoff();
    this.updateState();

    //TODO: доработать созжание Слайдера в паках
    if (name === 'fpacks') {
      this.slider = new Swiper('#fpacks_slider', {
        pagination: {
          el: '.swiper-pagination',
        },
        navigation: {
          nextEl: '.swiper-button-next-m',
          prevEl: '.swiper-button-prev-m',
        },
        //centeredSlides: true,
        slidesPerView: 4,
        centerInsufficientSlides: true,
        breakpointsInverse: true,
        initialSlide: 1,
        breakpoints: {
          // when window width is >= 320px
          479: {
            slidesPerView: 'auto',
            spaceBetween: 0
          },
          // when window width is >= 480px
          767: {
            slidesPerView: 'auto',
            spaceBetween: 20
          },
          // when window width is >= 640px
          991: {
            slidesPerView: 'auto',
            spaceBetween: 20
          }
        }
        // loop: true,
      });
    };
    // this.setActiveVar()
  }







  Template.prototype.gatherOnoff = function () {
    var onoffs = {}
    var thiss = this;
    //собираем элементы, которые включаются и выключаются в настройках

    var groupName;
    $(this.el).find(':attr(\'^data-xsb-template-onoff\')').each(function (i, oneEl) {
      var oneElId = oneEl.dataset.xsbTemplateOnoff;
      // var oneElIdUniq = oneElId + '_' + Object.keys(onoffs).length;
      var newContr = new TemplateControllerOnOff(thiss, oneElId);
      groupName = newContr.getRadioGroupName();
      if (groupName) {
        if (!onoffs[groupName]) {
          onoffs[groupName] = {}
        }
        onoffs[groupName][oneElId] = newContr;
        // thiss.onOffSwitch(groupName, null, onoffs)
      } else {
        onoffs[oneElId] = newContr;
      }
    });
    return onoffs;
  }


  Template.prototype.onOffSwitch = function (grouName, active, onoffs) {
    onoffs = onoffs || this.onoffs;

    // console.log('active = ', active);
    console.log('grouName = ', grouName);
    var thiss = this;
    if (!onoffs || !onoffs[grouName] || !Object.keys(onoffs[grouName]).length) return;
    Object.keys(onoffs[grouName]).forEach(function (radioName) {
      var oneRadio = onoffs[grouName][radioName];
      activeFromData = (oneRadio.dataT['shown']) ? radioName : null;
      active = active || activeFromData;
      if (radioName === active) {
      // if (oneRadio.active) {
          oneRadio.setActive(true);
          oneRadio.$trigger.find('input:radio:first').prop('checked', true);
          oneRadio.$trigger.find('input:checkbox').prop('checked', true);
        } else {
          oneRadio.setActive(false);
          oneRadio.$trigger.find('input:radio:first').prop('checked', false);
          oneRadio.$trigger.find('input:checkbox').prop('checked', false);
        }
      }
    )
  }





  Template.prototype.getShown = function () {
    if (this.dataT.shown) {
      return true;
    } else {
      return false;
    }
  }



  Template.prototype.getActiveVar = function () {
    var act;
    if (!this.dataT['variant']) {
      act =  Object.keys(this.getVars())[0];
    } else {
      act = this.dataT['variant'];
    }
    return act;
  }





  Template.prototype.setActiveVar = function (varKey, scope) {
    var thiss = scope || this;
    varKey = varKey || thiss.getActiveVar();
    thiss.dataT['variant'] = varKey;
    // this._xsb.setData();
    Object.keys(thiss.elVars).forEach(function (oneVar, i) {
      var key = Object.keys(thiss.elVars)[i];
      if (key !== varKey) {
        // $(thiss.elVars[oneVar]).addClass('hidden');
      } else {
        // $(thiss.elVars[oneVar]).removeClass('hidden');
      }
    })
  }


  Template.prototype.getTemplate = function (name) {
    return $("[data-xsb-template-type='" + name + "']")[0];
  }






  Template.prototype.getVars = function (name) {
    var vars = {};
    var $allVars = $(this.el).find(':attr(\'^data-xsb-template-type-var\')');

    if (!$allVars.length) {
      vars['default'] = $(this.el);
      return vars;
    }

    $allVars.each(function (i, oneVar) {
      var name = oneVar.dataset.xsbTemplateTypeVar;
      vars[name] = oneVar;
    });
    return vars;
  }





  Template.prototype.updateState = function () {
    // this.updateContent();
    if (this.getShown()) {
      this.el.classList.remove('hidden');
    } else {
      this.el.classList.add('hidden');
    }
  }





  Template.prototype.moduleHighlight = function () {
    if (this.shown) m__scrollTo(this.el, 400);
    var thiss = this;
    setTimeout(function () {
      thiss.el.classList.add('glow');
    },100)
    setTimeout(function () {
      thiss.el.classList.remove('glow');
    }, 300)
  }


  Template.prototype.deleteMedia = function (medianame, el) {
    // delete el.parentElement.removeChild(el);
    this.dataT.medias
    //TODO: доделать удаление
  }




  Template.prototype.gatherMedia = function () {
    var medias = {}
    var thiss = this;
    //собираем медию

    // for (var currentVariant in this.elVars) {
    // var varEl = thiss.elVars[currentVariant];

    $(this.el).find(':attr(\'^data-xsb-template-media\')').each(function (i, oneMedia) {
      var oneMediaId = oneMedia.dataset.xsbTemplateMedia;
      if (!medias[oneMediaId]) medias[oneMediaId] = {};

      var oneMediaIdUniq;
      oneMediaIdUniq = oneMediaId + '_' + i + '_' + Object.keys(medias).length;
      var newDataT;
      try {
        newDataT = thiss.dataT['media'][oneMediaId][oneMediaIdUniq];
      } catch (e) {}
      try {
        if (!newDataT) newDataT = thiss.dataT['media'][oneMediaId];
      }
      catch (e) {
        console.log('e = ', e);
      }
      // if (oneMediaId !== 'bg') {
        medias[oneMediaId][oneMediaIdUniq] = new TemplateMedia(thiss, oneMediaId, oneMediaIdUniq, oneMedia, null, newDataT);
      // } else {
      //   medias['bg']['bg'] = new TemplateMedia(thiss, oneMediaId, 'bg', 'bg', null, newDataT);
      // }
    })

    // }
    var oneMediaBg = $(thiss.el).find('[data-xsb-template-media=\'bg\']')[0];
    if (oneMediaBg) {
      medias['bg'] = {}
      try {
        var newBgDataT = thiss.dataT['media']['bg'] || thiss._xsb.dataX.global.media.bg;
      } catch (e) {
          console.log('e = ', e);
      }
      // medias['bg']['bg'] = new TemplateMedia(thiss, 'bg', 'bg', oneMediaBg, null, newBgDataT);
    }
    return medias;
  }




  Template.prototype.gatherSliders = function () {

    if (!$(this.el).find(':attr(\'^data-xsb-template-slider\')').length) return;

    var thiss = this;
    var galls = {};

    // Object.keys(this.elVars).forEach(function (oneVarName, i) {
    // $(this.el).find(':attr(\'^data-xsb-template-media\')').each(function (i, oneVarEl) {
    $(this.el).find(':attr(\'^data-xsb-template-slider\')').each(function (i, oneSlider) {
      var oneSliderId = oneSlider.dataset.xsbTemplateSlider;
      var oneSliderNameUniq = oneSliderId + '_' + i;
      galls[oneSliderNameUniq] = new TemplateSlider(thiss, oneSliderId, oneSlider)
    });
    // });
    return galls;
  }





  Template.prototype.gatherEditors = function () {
    var thiss = this;
    var lang = this._xsb.dataX['global']['languages']['active'];
    var editors = {}

    //перебираем хтмл элементы и тянем из данных
    var thiss = this;

    // $(this.el).find(':attr(\'^data-xsb-template-media\')').each(function (i, varEl) {
    // for (var currentVariant in this.elVars) {
    //   var varEl = thiss.elVars[currentVariant];

      $(this.el).find(':attr(\'^data-xsb-template-content\')').each(function (i, oneTemplateEditorEl) {
        var contName = oneTemplateEditorEl.dataset.xsbTemplateContent;

        if (!editors[contName]) editors[contName] = {};


        var contNameUniq = contName + '_' + i + '_' + Object.keys(editors).length;;

        if (!thiss.dataT['content']) {
          thiss.dataT['content'] = {}
        };
        if (!thiss.dataT['content'][lang]) {
          thiss.dataT['content'][lang] = {};
        }
        if (!thiss.dataT['content'][lang][contName]) {
          thiss.dataT['content'][lang][contName] = {};
          // thiss.dataT['content'][lang][contName]['txt'] = thiss._xsb.defaults.dummyText[lang];
        }

        editors[contName][contNameUniq] = new TemplateEditor(thiss, contName, contNameUniq, oneTemplateEditorEl);
      });
    // })
    return editors;
  }

  return Template;
})