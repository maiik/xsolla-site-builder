/* global _*/
// 'use strict';



var version = '1jul2018-1';
var debug = false;

var local;
// var local = 'en_US';
// var local = 'ru_RU';



var siteBuilder;


requirejs.config({
  baseUrl:  baseURL || 'https://xsolla-sitebuilder.netlify.com/',
  paths: {
    'xsb-defaults': 'https://xsolla-sitebuilder.netlify.com/js/xsb-defaults',
    'xsb-howtos': 'https://xsolla-sitebuilder.netlify.com/js/xsb-howtos',
    // 'xsb-howtos-button': 'https://xsolla-sitebuilder.netlify.com/js/xsb-howtos-button',
  }
});







define([
  'xsb-defaults',
  'xsb-howtos',
], function (Defaults, HowTos) {


  function XsollaSiteBuilder() {
    // this.preloader();
    this.debug = debug;
    this.version = version;
    this.defaults = new Defaults();
    this._xsb = this;

    this.preloader('hide');
    this.howTos = new HowTos(this);
  }

  XsollaSiteBuilder.prototype.preloader = function (doo) {
    if (doo === 'hide') {
      // $('.xsb_prel').removeClass('xsb_prel_white');
      // $('.xsb_prel').removeClass('xsb_prel_shown');
      setTimeout(function () {
        $('.xsb_prel').addClass('hidden');
      }, 400);
    } else {
    }
  }




  window.siteBuilder = new XsollaSiteBuilder();
  return XsollaSiteBuilder;
});



















