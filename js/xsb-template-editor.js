/* global _*/
// 'use strict';



define([
  'quill'
], function (Quill) {


  /********************* EDITOR *********************/
  /********************* EDITOR *********************/
  /********************* EDITOR *********************/
  /********************* EDITOR *********************/


  function TemplateEditor(parent, contName, contNameUniq, templateEl) {
    this._xsb = parent._xsb;
    this._template = parent;
    this.contName = contName;
    this.contNameUniq = contNameUniq;
    this.quill;
    this.lang = this._xsb.dataX.global['languages']['active'];
    this.shown = true;
    this.editorData = this.checkContent(this._template.dataT['content']);
    this.templateEl = templateEl;
    this.el = this.getElement();
    this.contApi = this.getContent();
    this.elContent; //Actual text
    this.showHideButton;
    this.editor;
    this.createTemplateEditor();
    this.updateState();
    // this.changeText();
  }



  TemplateEditor.prototype.checkContent = function (allData) {
    var allData = allData || {};

    if (!allData[this._xsb.getLang()]) {
      allData[this._xsb.getLang()] = {};
    }

    editorData = allData[this._xsb.getLang()];


    var txt = this._xsb.defaults.dummyTitle[this.lang];

    if (this.contName  === 'title') {
      txt = this._xsb.defaults.dummyTitle[this.lang];
    } //просто чтобы заголовки были красивые
    if (this.contName  === 'txt') {
      txt = this._xsb.defaults.dummyText[this.lang];
    } //просто чтобы заголовки были красивые

    if (!editorData[this.contName ] ) {
      editorData[this.contName ] = {};
    }
    editorData[this.contName ]['txt'] = editorData[this.contName ]['txt'] || txt;

    // JSON.stringify(
      // [
      //   { insert: this.defaults.dummyText[this._xsb.dataX.global['languages']['active']]},
      //   { insert: '\n' }
      // ]
    // );
    this._xsb.setData();
    return editorData;
  }


  TemplateEditor.prototype.getElement = function () {
    // var ell = $(this.templateEl).find("[data-xsb-template-content='" + this.contName  + "']");
    var el = this.templateEl;

    if ($(el.childNodes).length) {
      el.innerHTML = '';
    }
    el = this.createTemplateEditorCont();
    return el;
  }


  TemplateEditor.prototype.createTemplateEditorCont = function () {
    var editorOuter = document.createElement('DIV');
    editorOuter.classList.add('xsb_editable');
    this.templateEl.appendChild(editorOuter);
    return editorOuter;
  }

  TemplateEditor.prototype.updateState = function (shown) {
    if (typeof shown === 'boolean') this.shown = shown;
    if (shown === 'toggle') this.shown = !this.shown;
    if (this.shown) {
      $(this.el.parentElement).find('.xsb_e_inner')[0].classList.remove('hidden');
      $(this.elContent)[0].classList.add('hidden');
    } else {
      $(this.el.parentElement).find('.xsb_e_inner')[0].classList.add('hidden');
      $(this.elContent)[0].classList.remove('hidden');
    }
  }



  TemplateEditor.prototype.checkJSON = function (json) {
    if (/^[\],:{}\s]*$/.test(json.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
      //the json is ok
      return true;
    }else {
      //the json is not ok
      return false;
    }
  }




  TemplateEditor.prototype.getContent = function () {
    var cont;
    // var cont = this.editorData[this.contName ][lang];
    // var cont = co[Object.keys(cont)[0]];
    if (this.checkJSON(this.editorData[this.contName]['txt'])) {
      return JSON.parse(this.editorData[this.contName]['txt']);
      // return this.editorData[this.contName ]['txt'];
    } else {
      cont = [
        { insert: this.editorData[this.contName ]['txt']},
        { insert: '\n' }
      ];
      return cont;
    }
  }

  TemplateEditor.prototype.setContent = function (newCont) {
    this.contApi = JSON.stringify(newCont);
    this.editorData[this.contName ]['txt'] = this.contApi;
    // this._xsb.dataX['modules'][this._template.templateName]['content'][this._xsb.language][this.contName ]['txt'] = this.contApi;
    // this.editorData[this.contName ]['txt'] = this.contApi;
    this._xsb.setData();
  }


  TemplateEditor.prototype.changeText = function (newCont) {
    // this.elContent.innerHTML = this.contApi;
    //this.elContent.innerHTML = this.el.querySelector('.ql-editor').innerHTML; //TODO: не используется
    var thiss = this;
    if (!this._template.editors) return;
    Object.keys(this._template.editors[this.contName]).forEach(function (oneEditorName, i) {
      var newContHTML = JSON.parse(thiss.contApi);
      // if (oneEditorName !== thiss.contNameUniq) {
        thiss._template.editors[thiss.contName][oneEditorName].quill.setContents(newContHTML, 'api');
      // }
    });
  }


  TemplateEditor.prototype.createTemplateEditor = function () {
    if (this.editor) return;

    //внешний контейнер
    var editorC = document.createElement('DIV');
    editorC.classList.add('xsb_e');
    this.el.appendChild(editorC);


    //контейнер редактора (скрывается, показывается)
    var editorInner = document.createElement('DIV');
    editorInner.classList.add('xsb_e_inner');
    editorC.appendChild(editorInner);

    //сам редактор (без тулбара)
    this.editor = document.createElement('DIV');
    this.editor.classList.add('xsb_editor');
    editorInner.appendChild(this.editor);


    //видимый контент
    var visibleContent = document.createElement('DIV');
    visibleContent.classList.add('xsb_visible_content');
    this.el.appendChild(visibleContent);
    this.elContent = visibleContent;



    // this.editor

    var Delta = Quill.import('delta');
    var textBeforeEdit = this.contApi;

    // var toolbarOptions = [['bold', 'italic'], ['link', 'image']];
    var toolbarOptions = [
      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      ['clean'],                                        // remove formatting button
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      // ['blockquote', 'code-block'],

      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      // [{ 'direction': 'rtl' }],                         // text direction

      // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

    ];
    this.quill = new Quill(this.editor, {
      modules: {
        toolbar: toolbarOptions,
      },
      placeholder: this._xsb.defaults.dummyTitle[this.lang],
      theme: 'bubble',
    });
    this.quill.setContents(this.contApi, 'api');
    // quill.removeFormat(0, 100);


    // Store accumulated changes
    var change = new Delta();
    this.quill.on('text-change', function (delta) {
      change = change.compose(delta);
    });
    // var whatWeChange = this._xsb.getData('modules'); //TODO: change

    // Save periodically
    setInterval(function () {
      if (change.length() > 0) {
        console.log('Saving changes', change);
        this.setContent(this.quill.getContents(change));
        this.changeText();
        /*
        Send partial changes
        $.post('/your-endpoint', {
          partial: JSON.stringify(change)
        });

        Send entire document
        $.post('/your-endpoint', {
          doc: JSON.stringify(this.quill.getContents())
        });
        */
        change = new Delta();
      }
    }.bind(this), 5 * 1000);

  }

  return TemplateEditor;
})