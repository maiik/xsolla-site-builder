/* global _*/
// 'use strict';



define([
  'js/xsb-settings-media-colorpicker',
  'js/xsb-background',
], function (ColorPickerMedia, Background) {



  function SettingsMedia(parent, medEl) {
    this._parent = parent;
    this._xsb = this._parent._xsb;
    // this.dataT = this._xsb.dataX['modules']
    this.el = medEl;
    this.mediaSelectEl = $(this.el).find('[data-xsb-settings-media=\'url\']')[0];
    this.mediaSelectUl = $(this.el).find('[data-xsb-settings-media=\'ul\']')[0];
    this.$mediaOptionsEl = $(this.el).find('[data-xsb-settings-media=\'style\']');
    this.colorPicker = {};
    this.targetMedia = null;
    this.info = this.getMediaInfo()
    this.medias = this.gatherMedias();
    this.setListeners();
  }

  SettingsMedia.prototype.getMediaInfo = function (newTarget) {
    return '<div class="xsb_sett_media_i_pop_info">' +
          '<div>logo_new...31.png</div>' +
          '</div>' +
          '<div class="xsb_sett_media_i_pop_info">' +
            '<div>1000 <span class="xsb_sett_media_i--delimeter">× </span>800 <span class="xsb_sett_media_i--delimeter">px</span></div>' +
          '</div>' +
          '<div class="xsb_sett_media_i_pop_info">' +
            '<div>800<span class="xsb_sett_media_i--delimeter">kB</span></div>' +
          '</div>'
  }

  SettingsMedia.prototype.setTarget = function (newTarget) {
    this.glow();
    if (!newTarget) {
      this.$mediaOptionsEl.addClass('hidden');
    } else {
      this.$mediaOptionsEl.removeClass('hidden');
    }
    this.targetMedia = newTarget ? newTarget : null;
    if (this.targetMedia) {
      this.colorPicker['picker'].targetMedia = this.targetMedia;
      this.colorPicker['picker'].setPickerColor();
    }
  }

  SettingsMedia.prototype.glow = function () {
    var $glow = $(this.el).find('[data-xsb-settings=\'glow\']');
    $glow.addClass('shown');
    setTimeout(function () {
      $glow.removeClass('shown');
    }, 200)
  }

  SettingsMedia.prototype.gatherMedias = function () {
    var thiss = this;
    var medias = {};
    this.mediaSelectUl.innerHTML = '';
    Object.keys(this._xsb.dataX.global.media).forEach(function (oneMediaId) {
      var oneThumbURL = thiss._xsb.dataX.global.media[oneMediaId]['thumb'];
      var oneMediaURL;
      if (oneThumbURL) {
        oneMediaURL = thiss._xsb.dataX.global.media[oneMediaId]['url'];
      } else {
        oneThumbURL = thiss._xsb.dataX.global.media[oneMediaId]['url'];
        oneMediaURL = thiss._xsb.dataX.global.media[oneMediaId]['url'];
      }

      if (typeof oneThumbURL === 'string' &&
        thiss._xsb.dataX.global.media[oneMediaId]['thumb'] !== false) {

        var oneMediaEl = document.createElement('LI');
        oneMediaEl.classList.add('xsb_sett_media_li');

        var oneMediaElBg = document.createElement('DIV');
        oneMediaElBg.classList.add('xsb_sett_media_li_bg');

        var oneMediaElInfo = '<div class="xsb_sett_media_i">' +
        '<div class="xsb_sett_media_i_hint">' +
          '<div class="xsb_hint xsb_hint--mediadel">' +
            thiss.info +
            '<div class="xsb_sett_media_i_pop_but xsb_sett_media_i_pop_but--delete">' +
              '<div>Delete</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
        '<div class="xsb_sett_media_i_icon active">' +
          '<div>settings</div>' +
        '</div>';

        // var oneMediaElInfo = '<div class="xsb_sett_media_i">' +
        //     '<div class="xsb_sett_media_i_icon active">' +
        //       '<div>delete</div>' +
        //     '</div>' +
        //   '</div>';

        oneMediaEl.innerHTML = oneMediaElInfo;
        oneMediaEl.appendChild(oneMediaElBg);

        var $infoTrigger = $(oneMediaEl).find('.xsb_sett_media_i');
        var $infoBody = $(oneMediaEl).find('.xsb_sett_media_i_hint');
        var $infoText = $(oneMediaEl).find('.xsb_hint');
        var $onmel = $(oneMediaEl);
        var shown = false;

        $infoTrigger.on({
          click: function (evt) {
            shown = !shown;
            var xy = $infoTrigger.offset();
            $infoBody.toggleClass('shown');
            if (shown) {
              $infoTrigger.offset(xy);
              $infoTrigger.appendTo('body');
              $infoTrigger.addClass('shown');
            } else {
              $infoTrigger.removeClass('shown');
              // $infoTrigger.offset({ top: 0, left: 0});
              $infoTrigger.appendTo($onmel);
            }
          },
          // mouseover: function (evt) {
          //   var xy = $infoTrigger.offset();
          //   $infoTrigger.toggleClass('shown');
          //   $infoBody.toggleClass('shown');
          //   $infoTrigger.appendTo('body');
          //   $infoTrigger.offset(xy);
          // },
          // mouseout: function (evt) {
          //   var xy = $infoTrigger.offset();
          //   $infoTrigger.toggleClass('shown');
          //   $infoBody.toggleClass('shown');
          //   var onmel = $(oneMediaEl);
          //   $infoTrigger.appendTo(onmel);
          //   $infoTrigger.offset({ top: 0, left: 0});
          // },
        });

        $(oneMediaElBg).css('background-image', 'url(\'' + oneThumbURL + '\')');
        medias[oneMediaId] = {};
        medias[oneMediaId]['id'] = oneMediaId;
        medias[oneMediaId]['el'] = oneMediaEl;
        thiss.mediaSelectUl.appendChild(oneMediaEl);
      };
    });
    medias['clear'] = {};
    medias['clear']['id'] = 'blank';
    medias['clear']['el'] = $(this.el).find('[data-xsb-settings-media=\'clear\']')[0];

    return medias;
  }



  SettingsMedia.prototype.setListeners = function () {
    var thiss = this;
    Object.keys(this.medias).forEach(function (oneMediaId, i) {
      var oneMedia = thiss.medias[oneMediaId];
      $(oneMedia['el']).on({
        click: function (evt) {
          thiss.setImage(oneMedia['id'], 'save');
        },
        mouseover: function (evt) {
          if (oneMediaId !== 'clear') {
            thiss.setImage(oneMedia['id']);
          }
        },
        mouseout: function (evt) {
          if (oneMediaId !== 'clear') {
            thiss.setImage();
          }
        },
      });
    })


    var $styleRadios = $(this.el).find(':attr(\'^data-xsb-settings-media-style\')');
    // if (thiss.targetMedia) $styleRadios.css({ 'background-image': 'url(\'' + thiss.targetMedia.dataT['url'] + '\')' });
    $styleRadios.each(function (i, oneRadio) {

      $(oneRadio).on({
        click: function (evt) {
          $styleRadios.removeClass('active');
          var newStyle = oneRadio.dataset.xsbSettingsMediaStyle.split(',');
          var newStyleName = newStyle[0];
          var newStyleVal = newStyle[1];
          if (thiss.targetMedia) thiss.targetMedia.updateStyle(newStyleName, newStyleVal, 'save');
          $(oneRadio).find('input:radio:first').prop('checked', true);
          $(oneRadio).addClass('active');
        },
        // mouseover: function (evt) {
        //   var newStyle = oneRadio.dataset.xsbControlPanelModuleStyle.split(',');
        //   var newStyleName = newStyle[0];
        //   var newStyleVal = newStyle[1];
        //   if (thiss.targetMedia) thiss.targetMedia.updateStyle(newStyleName, newStyleVal);
        // },
        // mouseout: function (evt) {
        //   var newStyle = oneRadio.dataset.xsbControlPanelModuleStyle.split(',');
        //   var newStyleName = newStyle[0];
        //   var newStyleVal = newStyle[1];
        //   if (thiss.targetMedia) thiss.targetMedia.updateStyle(newStyleName, newStyleVal);
        // },
      });
    });

    var $tintCont = $(this.el).find(':attr(\'^data-xsb-settings-media-tint\')');
    $tintCont.each(function (i, oneTint) {
      var oneTintName = oneTint.dataset.xsbSettingsMediaTint;
      thiss.colorPicker[oneTintName] = new ColorPickerMedia(thiss, oneTint);
    });
  }

  SettingsMedia.prototype.onChangeCallback = function (newColor) {
    if (this.targetMedia) this.targetMedia.setTint(newColor, 'save');
  }




  SettingsMedia.prototype.setImage = function (oneMediaId, save) {
    oneUrl = oneMediaId;
    if (this.targetMedia) this.targetMedia.setImage(oneMediaId, save)
  }

  return SettingsMedia;
})